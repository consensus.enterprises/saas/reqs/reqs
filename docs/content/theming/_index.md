---
title: Theming

---

Since Reqs is built with Drupal, the presentation layer is handled with a "theme"; in this case, `reqs_theme`.

To manage CSS efficiently, we use [SASS](https://sass-lang.com/). The first thing you'll need to do is install it, which we can do with [Drumkit](https://drumk.it):
```
make sass
```

To run Sass, you have to be in the theme directory:
```
cd web/profiles/reqs_distro/themes/reqs_theme
sass sass:css --watch --no-source-map
```
