include .mk/GNUmakefile

INSTALL_PROFILE=reqs_distro

all: start build install

dev: install
	$(DRUSH) pm-enable -y reqs_devel
