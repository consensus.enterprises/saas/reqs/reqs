#!/bin/bash

# Make our base URL (and chromedriver) resolve.
echo `hostname -I` reqs.lndo.site chromedriver >> /etc/hosts

# Make our directory readable by the web user.
chmod -R 755 ./web
chown -R www-data ./web

# Copy our codebase to a valid docroot
cp -R . /var/www/reqs

# Deploy our vhosts.
cp scripts/apache/reqs.lndo.site.conf /etc/apache2/sites-available/
ln -s /etc/apache2/sites-available/reqs.lndo.site.conf /etc/apache2/sites-enabled/reqs.lndo.site.conf

# Start the web server
service apache2 start
