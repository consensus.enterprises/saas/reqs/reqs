<?php

namespace Drupal\reqs;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Helper to look up term IDs for default value functions.
 */
class TermReferenceDefault {

  /* Default value for fields referencing the 'Step Type' taxonomy. */
  protected $defaultStepType = 'Given';

  /* Default value for fields referencing the 'Importance' taxonomy. */
  protected $defaultImportance = 'want';

  /* Drupal\Core\Entity\ContentEntityInterface */
  protected $entity;

  /* Drupal\field\Entity\FieldConfig */
  protected $field;

  /**
   * Constructor method.
   *
   * @param ContentEntityInterface $entity
   *   A content entity; most likely a Paragraph.
   * @param FieldConfig $field
   *   Configuration for a taxonomy term reference field.
   */
  public function __construct(ContentEntityInterface $entity, FieldConfig $field) {
    # N.B. These aren't used at the moment, but are passed into the default
    # value function, and may be handy later.
    $this->entity = $entity;
    $this->field = $field;
  }

  /**
   * Instantiates this class and delegates to the proper method.
   *
   * @see: field.field.paragraph.step.field_step_type
   */
  public static function getStepType(ContentEntityInterface $entity, FieldConfig $field) {
    $default = new static($entity, $field);
    return $default->getDefaultStepType();
  }

  /**
   * Returns the term ID of the default for the 'Step Type' taxonomy.
   */
  public function getDefaultStepType() {
    return $this->getTermIdFromName($this->defaultStepType);
  }

  /**
   * Instantiates this class and delegates to the proper method.
   *
   * @see: field.field.paragraph.story.field_importance
   */
  public static function getImportance(ContentEntityInterface $entity, FieldConfig $field) {
    $default = new static($entity, $field);
    return $default->getDefaultImportance();
  }

  /**
   * Returns the term ID of the default for the 'Importance' taxonomy.
   */
  protected function getDefaultImportance() {
    return $this->getTermIdFromName($this->defaultImportance);
  }

  /**
   * Returns the taxonomy term ID for a given name.
   */
  function getTermIdFromName($name) {
    $terms = \Drupal::service('entity.manager')
      ->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $name]);
    return reset($terms)->id();
  }

}
