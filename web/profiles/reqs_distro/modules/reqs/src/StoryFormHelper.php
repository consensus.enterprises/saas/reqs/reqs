<?php

namespace Drupal\reqs;

use Drupal\Core\Form\FormStateInterface;

/**
 * Helper to interact with forms.
 */
class StoryFormHelper {

  /* A form array. */
  protected $form;

  /* A form state object. */
  protected $formState;

  /**
   * Constructor method.
   *
   * @param array &$form;
   *   A form array, passed by reference.
   * @param FormStateInferface $form_state;
   *   A form state object.
   */
  public function __construct(array &$form, FormStateInterface $form_state) {
    $this->form = $form;
    $this->formState = $form_state;
  }

  /**
   * Return the altered form.
   */
  public function getAlteredForm() {
    return $this->alter()->form;
  }

  /**
   * Alter the form.
   */
  protected function alter() {
    return $this
      ->disableAutocomplete()
      ->AttachCssAndJs()
      ->makeStoryDependOnTitle()
      ->AddStoryFieldDependency('field_task','field_goal')
      ->AddStoryFieldDependency('field_importance','field_goal')
      ->AddStoryFieldDependency('field_goal','field_role')
      ->makeScenariosDependOnTitle();
  }

  protected function disableAutocomplete() {
    $this->form['#attributes']['autocomplete'] = 'off';
    return $this;
  }

  protected function AttachCssAndJs() {
    $this->form['#attached']['library'][] = 'reqs/reqs_form';
    return $this;
  }

  protected function makeStoryDependOnTitle() {
    $this->form['field_story']['#states'] = [
      'invisible' => [
        ':input[name="title[0][value]"]' => [
          'empty' => TRUE,
        ],
      ],
    ];
    return $this;
  }

  protected function AddStoryFieldDependency($dependent, $dependee) {
    $story = $this->getStory();
    $story[$dependent]['#states'] = [
      'invisible' => [
        ":input[name=\"field_story[0][subform][{$dependee}][0][target_id]\"]" => [
          'empty' => TRUE,
        ],
      ],
    ];
    $this->setStory($story);
    return $this;
  }

  protected function getStory() {
    return $this->form['field_story']['widget']['0']['subform'];
  }
  protected function setStory($story) {
    $this->form['field_story']['widget']['0']['subform'] = $story;
  }

  protected function makeScenariosDependOnTitle() {
    $this->form['field_scenarios']['#states'] = [
      'invisible' => [
        ':input[name="title[0][value]"]' => [
        // @TODO Make this depend on the User story being complete.
        #':input[name="field_story[0][subform][field_task][0][target_id]"]' => [
          'empty' => TRUE,
        ],
      ],
    ];
    return $this;
  }

}

