<?php

namespace Drupal\reqs;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Helper to interact with Paragraph widgets.
 */
class ParagraphsWidgetHelper {

  use StringTranslationTrait;

  /* Array with actions and dropdown widget actions. */
  protected $widget_actions;

  /* Array containing context for the widget. */
  protected $context;

  /**
   * Constructor method.
   *
   * @param array $widget_actions
   *   Array with actions and dropdown widget actions.
   * @param array $context
   *   An associative array containing the following key-value pairs:
   *   - form: The form structure to which widgets are being attached. This may be
   *     a full form structure, or a sub-element of a larger form.
   *   - widget: The widget plugin instance.
   *   - items: The field values, as a
   *     \Drupal\Core\Field\FieldItemListInterface object.
   *   - delta: The order of this item in the array of subelements (0, 1, 2, etc).
   *   - element: A form element array containing basic properties for the widget.
   *   - form_state: The current state of the form.
   *   - paragraphs_entity: the paragraphs entity for this widget. Might be
   *     unsaved, if we have just added a new item to the widget.
   *   - is_translating: Boolean if the widget is translating.
   *   - allow_reference_changes: Boolean if changes to structure are OK.
   */
  public function __construct(array &$widget_actions, array &$context) {
    $this->widget_actions = $widget_actions;
    $this->context = $context;
  }

  public function getAlteredWidgetActions() {
    return $this->alterWidgetActions()->widget_actions;
  }

  public function getAlteredContext() {
    return $this->alterContext()->context;
  }

  /**
   * Alter widget actions.
   */
  protected function alterWidgetActions() {
    return $this
      ->setAttributesForDropdownActions()
      ->moveActionsFromDropdown()
      ->removeUnwantedActions()
      ->setIconsForActions();
  }

  /**
   * Alter widget context.
   */
  protected function alterContext() {
    return $this;
  }

  protected function setAttributesForDropdownActions() {
    foreach ($this->widget_actions['dropdown_actions'] as $name => &$action) {
      $type = strtolower($action['#value']);
      $action['#attributes'] = [
        'class' => [
          'paragraphs-icon-button',
          'paragraphs-icon-button-' . $type,
        ],
        'title' => $action['#value'],
      ];
    }
    return $this;
  }

  protected function moveActionsFromDropdown() {
    foreach ($this->widget_actions['dropdown_actions'] as $name => $action) {
      $this->widget_actions['actions'][$name] = $action;
      unset($this->widget_actions['dropdown_actions'][$name]);
    }
    return $this;
  }

  protected function removeUnwantedActions() {
    switch ($this->getParagraphType()) {
      case 'step':
        // Steps are single lines, so collapsing them doesn't make sense.
        return $this->removeUnwantedAction('collapse_button');
      default:
        return $this;
    }
  }

  protected function getParagraphType() {
    return $this->context['element']['#paragraph_type'];
  }

  protected function removeUnwantedAction($action) {
    unset($this->widget_actions['actions'][$action]);
    return $this;
  }

  protected function setIconsForActions() {
    foreach ($this->widget_actions['actions'] as $name => &$action) {
      switch ($name) {
        case 'remove_button':
          $fa_icon = 'fa-trash-alt';
          break;
        case 'collapse_button':
          $fa_icon = 'fa-chevron-circle-up';
          break;
        case 'duplicate_button':
          $fa_icon = 'fa-copy';
          break;
        case 'edit_button':
          $fa_icon = 'fa-chevron-circle-down';
          break;
 
      }
      $action['#prefix'] = '<div class="paragraphs-icon-button-wrapper"><i class="fas ' . $fa_icon . '"></i>';
      $action['#suffix'] = '</div>';
    }
    return $this;
  }

}

