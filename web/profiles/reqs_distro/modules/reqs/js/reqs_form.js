// @TODO Adjust "a/an" on role field label depending on whether the first letter is a vowel.

function processFields() {
  var text_fields = document.querySelectorAll('.form-text'); // get the input elements
  text_fields.forEach((input) => {
    // Resize as the user types.
    input.addEventListener('input', resizeInput);
    // Resize as soon as an autocomplete option is chosen.
    // @TODO 'mouseup' doesn't appear to be capturing the event when an autocomplete option is clicked.
    input.addEventListener('keyup', resizeInput);
    input.addEventListener('mouseup', resizeInput);
    // Resize as soon as the user moves to the next field.
    input.addEventListener('blur', resizeInput);
    // Initially resize to fit placeholder text.
    resizeInput.call(input);
  });
}

function resizeInput() {
  content_width = this.value.length;
  placeholder_width = this.placeholder.length;
  if (content_width > placeholder_width) {
    this.style.width = this.value.length + 2 + "ch";
  }
  else {
    this.style.width = this.placeholder.length + 2 + "ch";
  }
}

Drupal.behaviors.reqsBehavior = {
  attach: function (context, settings) {
    processFields();
  }
};

