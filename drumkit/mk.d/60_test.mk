# Run our test suite.

.PHONY: tests tests-wip vnc test-steps

prune-test-cruft:
	ddev stop
	docker network prune
	docker image prune
	docker container prune
	docker volume prune

tests: tests-prep ## Run functional Behat test suite
	$(BEHAT) --stop-on-failure

tests-prep:
#	$(DRUSH) config-enforce:enforce

tests-migrate: migrate-prep ## Run migration Behat test suite
	$(BEHAT) --stop-on-failure --profile=migrate

test-wip:
	$(BEHAT) --stop-on-failure --tags=wip
tests-wip: test-wip

test-steps:
	$(BEHAT) -dl

# Set the VNC password for chromedriver (`secret`)
~/.stc/vncpasswd:
	@mkdir -p ~/.stc
	@echo "secret" | vncpasswd -f > ~/.stc/vncpasswd

# @TODO: set an ENV variable for users *actual* vncviewer?
vnc: ~/.stc/vncpasswd ## Spawn xvncviewer to see local chromedriver running.
	@$(ECHO) "$(YELLOW)Beginning spawn of xvncviewer in background.$(RESET)"
	@xvncviewer localhost:5900 -passwd ~/.stc/vncpasswd &
	@$(ECHO) "$(YELLOW)Completed spawn of xvncviewer in background.$(RESET)"
