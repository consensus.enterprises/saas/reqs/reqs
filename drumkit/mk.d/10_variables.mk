SITE_URL  = reqs.ddev.site
SITE_NAME = "Reqs.app"

ADMIN_USER = dev
ADMIN_PASS = pwd
INSTALL_PROFILE = reqs_distro

DB_NAME = db
DB_USER = db
DB_PASSWORD = db
DB_HOST = db
DB_PORT = 3306

TIMESTAMP = $(shell date +%s)
TMP_DIR   = tmp

VCS_GROUP_NAMESPACE = NRCan
PROJECT_NAME = pmt
CLIENT_NAME = nrcan

# Suppress Make-specific output, but allow for greater verbosity.
VERBOSE ?= 0
QUIET   :=
ifeq ($(VERBOSE), 0)
    MAKE-QUIET = $(MAKE) -s
    QUIET      = > /dev/null
    DRUSH_VERBOSE =
else
    MAKE-QUIET = $(MAKE)
    DRUSH_VERBOSE = --verbose
endif

# Allow debug output
DEBUG ?= 0
ifeq ($(DEBUG), 0)
    DRUSH_DEBUG =
else
    DRUSH_DEBUG = --debug
endif

# Normalize local development and CI commands.
DDEV = $(shell which ddev)
ifeq ($(DDEV),)
    DRUSH_CMD = ./bin/drush --ansi
    DRUSH8 = ./.ddev/commands/web/drush8
    BEHAT_CMD = ./bin/behat
    APP_PATH  =
    COMPOSER  = composer --ansi
    DDEV_EXEC =
    DDEV_DB_EXEC =
else
    DRUSH_CMD = ddev exec drush --ansi
    DRUSH8 = ddev drush8
    BEHAT_CMD = ddev exec ./bin/behat
    APP_PATH  = /app/
    COMPOSER  = ddev exec composer --ansi
    DDEV_EXEC = $(DDEV) exec
    DDEV_DB_EXEC = $(DDEV) exec --service db
endif
DRUSH = $(DRUSH_CMD) --uri=$(SITE_URL) $(DRUSH_VERBOSE) $(DRUSH_DEBUG)
DRUSH_INSTALL = $(DRUSH_CMD) $(DRUSH_VERBOSE) $(DRUSH_DEBUG)
BEHAT = $(BEHAT_CMD) --colors

make = $(MAKE) -s
kubectl = kubectl --kubeconfig=.kube/config

# Colour output. See 'help' for example usage.
ECHO       = @echo -e
BOLD       = \033[1m
RESET      = \033[0m
make_color = \033[38;5;$1m  # defined for 1 through 255
GREEN      = $(strip $(call make_color,22))
GREY       = $(strip $(call make_color,241))
RED        = $(strip $(call make_color,124))
WHITE      = $(strip $(call make_color,255))
YELLOW     = $(strip $(call make_color,94))
LIME       = $(strip $(call make_color,10))
LEMON      = $(strip $(call make_color,11))
